'use strict';
const _ = require('lodash');
const expect = require('chai').expect;
const describe = require('mocha').describe;
const it = require('mocha').it;
const Nightmare = require('nightmare');

//use this package to run the UI test code under xvfb device virtualization
const Xvfb = require('xvfb');
const xvfb = new Xvfb({
    silent: true
});

const testEnv = require('./testEnv');
const testUrl  = testEnv.testUrl;

describe('GUI Tests', function () {
    this.timeout('20s');
    let app = null;
    let nightmare = null;
    before(() => {
        if(!testEnv.isContainerTest) app = require('../app');
        nightmare = new Nightmare()
    });
    after(() => {
        if(app) app.shutdown();
        if(!testEnv.isLocal) xvfb.stopSync();
    });

    it('Can login to GUI', done => {
        // code that uses the virtual frame buffer here, only
        // if the application is NOT running on a local machine
        if(!testEnv.isLocal) xvfb.startSync();
        //start the web server
        nightmare.goto(`${testUrl}/`)
            .wait('#btnlogin')
            .type('#txtUserName', 'testUser')
            .type('#txtPassword', 'cheese')
            .click('#btnlogin')
            .wait('#divWelcome')
            .evaluate(() => document.querySelector('#divWelcome').innerHTML)
            .end()
            .then(data => {
                expect(data).to.equal('Welcome testUser');
                done()
            })
            .catch(ex => {
                console.log(ex);
                done();
            })
    })
});